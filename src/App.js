import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import LayoutComponents from "./components/LayoutComponents";
import Home from "./components/Home";
import Employees from "./components/Employees";
import Departments from "./components/Departments";
import "./App.css";
import "antd/dist/antd.min.css";

function App() {
	const renderRoutes = () => {
		return (
			<Routes>
				<Route exact path="/" element={<Home />} />
				<Route path="/empleados" element={<Employees />} />
				<Route path="/departamentos" element={<Departments />} />
			</Routes>
		);
	};
	return (
		<Router>
			<LayoutComponents>{renderRoutes()}</LayoutComponents>
		</Router>
	);
}

export default App;
