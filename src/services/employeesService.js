import axios from 'axios'

export const loadEmployees = async () => {
  const data = await axios.get(`${process.env.REACT_APP_APIURL}/employee`)
  return data.data
}

export const saveEmploye = async (payload) => {
  const data = await axios.post(`${process.env.REACT_APP_APIURL}/employee`, payload)
  return data.data
}

export const updateEmploye = async (payload) => { 
  const data = await axios.put(`${process.env.REACT_APP_APIURL}/employee/${payload.id}`, payload)
  return data.data
}