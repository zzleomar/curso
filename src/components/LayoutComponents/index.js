import { Layout } from "antd";
import React from "react";
import Footer from "../Footer";
import Menu from "../Nav";
import Breadcrumb from "../Breadcrumb";
import "./styles.css";

const { Header, Content } = Layout;

function LayoutComponent(props) {
	return (
		<Layout className="layout">
			<Header>
				<div className="logo" />
				<Menu />
			</Header>
			<Content className="container-content">
				<Breadcrumb className="breadcrumb" />
				<div className="content">{props.children}</div>
			</Content>
			<Footer />
		</Layout>
	);
}

export default LayoutComponent;
