import { Space, Table, Tag, Button, notification, Avatar } from "antd";
import React, { useState, useEffect } from "react";
import ModalEmployees from '../ModalEmployees'
import { loadEmployees, saveEmploye, updateEmploye } from "../../services/employeesService";
import { loadLanguage } from "../../services/languageService";
import { loadDepartments } from "../../services/departmentsService";

const App = () => {
	const [data, setData] = useState([]);
  const [open, setOpen] = useState(false)
  const [item, setItem] = useState(null)
	const [languages, setLanguages] = useState([]);
	const [departments, setDepartments] = useState([]);

	const loadData = async () => {
    const employeesData = await loadEmployees();
    console.log(employeesData, 'employeesData')
		setData(employeesData);
		const languagesData = await loadLanguage();
		setLanguages(languagesData);
		const departmentsData = await loadDepartments();
		setDepartments(departmentsData);
	};

	useEffect(() => {
		loadData();
  }, []);

  const openNotificationSuccess = (message) => {
    notification.success({
      message: 'Bien hecho',
      description: message,
      placement: 'top',
    });
  };
  
  const handleEdit = (record) => {
    setItem(record)
    setOpen(true)
  }

  const handleNewItem = () => {
    setItem(null)
    setOpen(true)
  }

  const handleDelete = (record) => {
    const auxData = data.filter((item) => item.id !== record.id)
    setData(auxData)
    openNotificationSuccess("Empleado eliminado")

  }

  const onNewEmployee = async (payload) => {
    const newEmployee = await saveEmploye(payload);
    const auxData = [...data]
    auxData.unshift({
      ...newEmployee
    })
    setData(auxData)
    openNotificationSuccess("Empleado guardado")
  }

  const onEditEmployee = async (payload) => {
    const dataEmployee = await updateEmploye(payload)
    const auxData = [...data]
    const index = auxData.findIndex((item) => item.id === payload.id)
    auxData[index] = {
      ...dataEmployee
    }
    setData(auxData)
    openNotificationSuccess("Empleado modificado")
  }

  const handleSave = async (payload) => {
    if (item === null) {
      onNewEmployee(payload)
    } else {
      onEditEmployee({...payload, id: item.id })
    }
    handleHiddenModal()
  }

  const handleHiddenModal = () => {
    setItem(null)
    setOpen(false)
  }

const columns = [
  {
    title: "Nombre",
    dataIndex: "name",
    sorter: (a, b) => a.name.length - b.name.length,
    key: "name",
    render: (data, record) => <Space size="middle">
      <Avatar
        src={record.img}
      />
      {record.name}
    </Space>,
	},
	{
		title: "Lenguaje",
		dataIndex: "language",
    key: "language",
    defaultSortOrder: 'descend',
		render: (data) => <p>{data.name}</p>,
	},
  {
    title: "Departamentos",
    key: "departments",
    dataIndex: "departments",
    width: '100px',
    render: (_, { departments }) => (
      <div>
				{departments.map((department) => {
					return <Tag key={department.id}>{department.name.toUpperCase()}</Tag>;
				})}
			</div>
		),
	},
	{
		title: "Acciones",
		key: "action",
		render: (_, record) => (
			<Space size="middle">
				<Button type="link" block onClick={() => handleEdit(record)}>
					Editar
				</Button>
				<Button type="link" block danger onClick={() => handleDelete(record)}>
					Eliminar
				</Button>
			</Space>
		),
	},
];


  return <>
    <Button type="primary" onClick={() => handleNewItem()} className="btn-new">Nuevo empleado</Button>
    <Table columns={columns} dataSource={data} />
    <ModalEmployees isModalOpen={open} handleOk={handleSave} handleCancel={handleHiddenModal} employe={item} languages={languages} departments={departments} />
  </>
};

export default App;
