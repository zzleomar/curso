import { Menu } from 'antd';
import { useNavigate } from "react-router-dom";
import React from 'react';

function Nav() {
  let navigate = useNavigate();
  const options = [
    { name: 'Home', path: '/'},
    { name: 'Empleados', path: '/empleados'},
    { name: 'Departamentos', path: '/departamentos'},
  ]
  return <Menu
  theme="dark"
  mode="horizontal"
  items={options.map((item) => {
    return {
      key: `${item.path}`,
      label: `${item.name}`,
    };
  })}
  onClick={({ item, key, keyPath, domEvent }) => navigate(key)}
/>;
}

export default Nav;