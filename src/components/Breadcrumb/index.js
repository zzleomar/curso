import { Breadcrumb } from "antd";
import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";

function BreadcrumbComponent() {
	const location = useLocation();
	const [nameLocation, setNameLocation] = useState("Home");
	const options = [
		{ name: "Home", path: "/" },
		{ name: "Empleados", path: "/empleados" },
		{ name: "Departamentos", path: "/departamentos" },
	];
	useEffect(() => {
		const option = options.find((item) => location.pathname === item.path);
    setNameLocation(option.name);
	}, [location]);

	return (
		<Breadcrumb className="breadcrumb">
			<Breadcrumb.Item>Home</Breadcrumb.Item>
			{nameLocation !== "Home" && (
				<Breadcrumb.Item>{nameLocation}</Breadcrumb.Item>
			)}
		</Breadcrumb>
	);
}

export default BreadcrumbComponent;
