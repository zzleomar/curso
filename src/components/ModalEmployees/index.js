import { Form, Input, Modal, Select, Upload, message } from "antd";
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import React, { useState, useEffect } from "react";
const { Option } = Select;

const App = ({ isModalOpen, handleOk, handleCancel, employe, languages, departments }) => {
	const [form] = Form.useForm();
	const [dataDefault, setDataDefault] = useState(null);
	const [loading, setLoading] = useState(false);
  const [imageUrl, setImageUrl] = useState(null);

	const onFormDataChange = (data) => {
		setDataDefault(data);
	};
  
  const onOk = async () => {
		form.validateFields().then((values) => {
      handleOk({...values, img: imageUrl})
      setDataDefault(null)
    })
  }

  useEffect(() => {
    if (isModalOpen) {
      if (employe === null) {
        setDataDefault(null)
				form.resetFields()
				setImageUrl(null)
      } else {
        setDataDefault({ ...employe, departmentsId: employe.departments.map((item) => item.id) })
				form.setFieldsValue({ ...employe, departmentsId: employe.departments.map((item) => item.id) })
				setImageUrl(employe.img)
      }
		} else {
			setDataDefault(null)
			form.resetFields()
			setImageUrl(null)
		}
	}, [isModalOpen])
	
	const getBase64 = (img, callback) => {
		const reader = new FileReader();
		reader.addEventListener('load', () => callback(reader.result));
		reader.readAsDataURL(img);
	};

	const beforeUpload = (file) => {
		const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
		if (!isJpgOrPng) {
			message.error('You can only upload JPG/PNG file!');
		}
		const isLt2M = file.size / 1024 / 1024 < 2;
		if (!isLt2M) {
			message.error('Image must smaller than 2MB!');
		}
		return false;
	};

	const handleChangeImg = (info) => {
		getBase64(info.fileList[0].originFileObj, (url) => {
			setLoading(false);
			setImageUrl(url);
		});
    if (info.file.status === 'uploading') {
      setLoading(true);
      return;
    }
	};
	
	const uploadButton = (
    <div>
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div
        style={{
          marginTop: 8,
        }}
      >
        Upload
      </div>
    </div>
  );

	return (
		<>
			<Modal
				title={
					employe === null ? "Registro de empleados" : "Datos del empleado"
				}
				open={isModalOpen}
        onOk={onOk}
				onCancel={handleCancel}
			>
				<Form
					layout={"vertical"}
					form={form}
					initialValues={dataDefault}
					onValuesChange={onFormDataChange}
				>
					<Form.Item name="img" hasFeedback>
						<Upload
							name="avatar"
							listType="picture-card"
							className="avatar-uploader"
							showUploadList={false}
							beforeUpload={beforeUpload}
							onChange={handleChangeImg}
							maxCount={1}
						>
							{imageUrl ? (
								<img
									src={imageUrl}
									alt="avatar"
									style={{
										width: '100%',
									}}
								/>
							) : (
								uploadButton
							)}
						</Upload>
					</Form.Item>
					<Form.Item label="Nombre Completo" name="name" hasFeedback
						rules={[{ required: true, message: "Debe ingresar el nombre del empleado" }]}>
						<Input placeholder="ingrese el nombre del empleado" />
					</Form.Item>
					<Form.Item
						name="language_id"
						label="Lenguaje del empleado"
						hasFeedback
						rules={[{ required: true, message: "Seleccione una opción" }]}
					>
						<Select placeholder="Seleccione un Lenguaje">
							{languages.map((item) => (
								<Option value={item.id} key={item.id}>{item.name}</Option>
							))}
						</Select>
					</Form.Item>
					<Form.Item
						name="departmentsId"
						label="Departamentos"
						hasFeedback
						rules={[
							{ required: true, message: "Seleccione almenos una opción" },
						]}
					>
						<Select placeholder="Seleccione los departamentos" mode="multiple">
							{departments.map((item) => (
								<Option value={item.id} key={item.id}>{item.name}</Option>
							))}
						</Select>
					</Form.Item>
				</Form>
			</Modal>
		</>
	);
};

export default App;
